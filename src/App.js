import React, { useState } from 'react';
import Todo from './components/Todo';
import TodoForm from './components/TodoForm';

const todos = [
  {
    id: '01',
    text: 'Baca buku React Uncover',
  },
  {
    id: '02',
    text: 'Makan Siang',
  },
  {
    id: '03',
    text: 'Main Game',
  },
];

const App = () => {
  const [arrayTodo, setArrayTodo] = useState(todos);

  const deleteButtonHandler = (event) => {
    const newTodos = arrayTodo.filter(
      (item) => item.id !== event.target.id,
    );

    setArrayTodo(newTodos);
  };

  const addTodoHandler = (text) => {
    const newTodos = [
      ...arrayTodo,
      {
        id: new Date().getTime().toString(),
        text,
      },
    ];

    setArrayTodo(newTodos);
  };

  return (
    <div className="container">
      {
        arrayTodo.map((todo) => (
          <Todo
            key={todo.id}
            id={todo.id}
            text={todo.text}
            deleteButtonHandler={deleteButtonHandler}
          />
        ))
      }
      <TodoForm addTodoHandler={addTodoHandler} />
    </div>
  );
};

export default App;
