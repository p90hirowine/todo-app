import React, { useState } from 'react';

const TodoForm = (props) => {
  const [inputTodo, setInputTodo] = useState('');
  const [errorMassages, setErrorMassages] = useState(null);

  const { addTodoHandler } = props;

  const inputChangeHandler = (event) => {
    setInputTodo(event.target.value);
  };

  const submitButtonHandler = (event) => {
    event.preventDefault();

    if (inputTodo.trim() === '') {
      setErrorMassages('Inputan todo tidak boleh kosong');
    } else {
      addTodoHandler(inputTodo);
      setInputTodo('');
    }
  };

  return (
    <form onSubmit={submitButtonHandler}>
      <div>
        <input type="text" placeholder="Add todo ..." value={inputTodo} onChange={inputChangeHandler} />
        {errorMassages && <small>{errorMassages}</small>}
        {console.log(inputTodo)}
      </div>
    </form>
  );
};

export default TodoForm;
