const Todo = (props) => {
  const { text, id, deleteButtonHandler } = props;

  return (
    <div className="todo">
      {text}
      <span id={id} onClick={deleteButtonHandler}>x</span>
    </div>
  );
};

export default Todo;
